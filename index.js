var CROSS = 'X';
var ZERO = 'O';
var EMPTY = ' ';

let currentPlayer = CROSS;
let gridSize = 3;
let winner = '';
let ai = false;
const GRID = [];

startGame();

function startGame () {
  gridSize = +prompt('Укажите размер поля', 3);
  ai = confirm('Подключить ИИ?');

  if (isNaN(gridSize) || gridSize < 3) {
    gridSize = 3;
  }

  renderGrid(gridSize);
  resetClickHandler();
}

/* ход компьютера */
function turnAI() {
  let emptyCells = getEmptyCells();
  if (emptyCells.length === 0) return;

  let randIndex = Math.floor(Math.random() * emptyCells.length);
  cellClickHandler(emptyCells[randIndex][0], emptyCells[randIndex][1]); 
}

/* обработчик нажатия на клетку */
function cellClickHandler (row, col) {
  if (GRID[row][col] !== EMPTY || winner) return;
  
  GRID[row][col] = currentPlayer;
  renderSymbolInCell(currentPlayer, row, col);
  checkWinner(row, col);
  getGameStatus();

  currentPlayer = currentPlayer === CROSS ? ZERO : CROSS;
  if(ai && currentPlayer === ZERO) {
    turnAI();
  }
}

/* получить статус игры - есть победитель, ничья */
function getGameStatus() {
  if (winner) {
    let winnerText = winner === CROSS ? 'Крестики' : 'Нолики';
    showMessage(`${winnerText} выиграли`);
  }
  if (!winner && getEmptyCells().length === 0) {
    showMessage('Победила дружба');
  }
}

/* получить строку */
function getRow(rowIndex) {
  let arr = [];
  for (let i = 0; i < gridSize; i++) {
    arr.push({ 'row': rowIndex, 'col': i, 'symbol': GRID[rowIndex][i] });
  }
  return arr;
}
/* получить столбец */
function getColumn(colIndex) {
  let arr = [];
  for (let i = 0; i < gridSize; i++) {
    arr.push({ 'row': i, 'col': colIndex, 'symbol': GRID[i][colIndex] });
  }
  return arr;
}
/* получить главную диагональ */
function getMainDiagonal() {
  let arr = [];
  for (let i = 0; i < gridSize; i++) {
    arr.push({ 'row': i, 'col': i, 'symbol': GRID[i][i] });
  }
  return arr;
}
/* получить второстепенную диагональ */
function getSecDiagonal() {
  let arr = [];
  for (let i = 0; i < gridSize; i++) {
    arr.push({ 'row': i, 'col': gridSize-1-i, 'symbol': GRID[i][gridSize-1-i] });
  }
  return arr;
}
/* проверка символа в ячейке */
function isCurrentPlayer(cellInfo) {
  return cellInfo.symbol === currentPlayer;
} 
/* определение победителя */
function checkWinner(rowIndex, colIndex) {
  let row = getRow(rowIndex);
  let col = getColumn(colIndex);
  let mainDiagonal, secDiagonal;

  if (row.every(isCurrentPlayer)) {
    winner = currentPlayer;
    showWinCombs(row);
  }
  if (col.every(isCurrentPlayer)) {
    winner = currentPlayer;
    showWinCombs(col);
  }
  if (rowIndex === colIndex) {
    mainDiagonal = getMainDiagonal();
    if (mainDiagonal.every(isCurrentPlayer)) {
      winner = currentPlayer;
      showWinCombs(mainDiagonal);
    }
  }
  if (colIndex === gridSize-1-rowIndex) {
    secDiagonal = getSecDiagonal();
    if (secDiagonal.every(isCurrentPlayer)) {
      winner = currentPlayer;
      showWinCombs(secDiagonal);
    }
  }
}

/* окраска выигрышной комбинации в красный цвет */
function showWinCombs(winComb) {
  for (let i = 0; i < gridSize; i++) {
    renderSymbolInCell(winComb[i].symbol, winComb[i].row, winComb[i].col, '#ff0000');
  } 
}

/* плучить массив с координатами пустых ячеек */
function getEmptyCells() {
  let emptyCellArr = [];
  for (let i = 0; i < gridSize; i++) {
    for (let j = 0; j < gridSize; j++) {
      if (GRID[i][j] === EMPTY) {
        emptyCellArr.push([i, j]);
      }
    }
  }
  return emptyCellArr;
}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler () {
  console.log('reset!');
  showMessage('');

  winner = '';
  currentPlayer = CROSS

  for (let i = 0; i < gridSize; i++) {
    GRID[i] = [];
    for (let j = 0; j < gridSize; j++) {
      GRID[i][j] = EMPTY;
      findCell(i, j).textContent = EMPTY;
    }
  }
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid (dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell (symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell (row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin () {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw () {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell (row, col) {
  findCell(row, col).click();
}
